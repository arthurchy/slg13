%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%% ICML 2013 EXAMPLE LATEX SUBMISSION FILE %%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Use the following line _only_ if you're still using LaTeX 2.09.
%\documentstyle[icml2013,epsf,natbib]{article}
% If you rely on Latex2e packages, like most moden people use this:
\documentclass{article}

% For figures
\usepackage{graphicx} % more modern
%\usepackage{epsfig} % less modern
\usepackage{subfigure}

% For citations
\usepackage{natbib}

% For math
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{mathtools}

% For algorithms
\usepackage{algorithm}
\usepackage{algorithmic}

% As of 2011, we use the hyperref package to produce hyperlinks in the
% resulting PDF.  If this breaks your system, please commend out the
% following usepackage line and replace \usepackage{icml2013} with
% \usepackage[nohyperref]{icml2013} above.
\usepackage{hyperref}

% Packages hyperref and algorithmic misbehave sometimes.  We can fix
% this with the following command.
\newcommand{\theHalgorithm}{\arabic{algorithm}}

\usepackage{xspace}
\usepackage{color}
\usepackage{enumitem}
\usepackage{booktabs}
\usepackage{listings}

% Employ the following version of the ``usepackage'' statement for
% submitting the draft version of the paper for review.  This will set
% the note in the first column to ``Under review.  Do not distribute.''
\usepackage[accepted]{icml2013} 
% Employ this version of the ``usepackage'' statement after the paper has
% been accepted, when creating the final version.  This will set the
% note in the first column to ``Proceedings of the...''
% \usepackage[accepted]{icml2013}

\newcommand{\probkb}{\textsc{ProbKB}\xspace}
\newcommand{\sherlock}{\textsc{Sherlock}\xspace}
\newcommand{\reverb}{\textsc{ReVerb}\xspace}
\newcommand{\textrunner}{\textsc{TextRunner}\xspace}
\newcommand{\alchemy}{\textsc{Alchemy}\xspace}
\newcommand{\tuffy}{\textsc{Tuffy}\xspace}
\newcommand{\felix}{\textsc{Felix}\xspace}
\newcommand{\nell}{\textsc{Nell}\xspace}

\newcommand{\stt}[1]{\texttt{\small#1}\xspace}

\newcommand{\yang}[1]{{\color{blue}(#1)}}

\newcommand{\eat}[1]{}

% spacing in display mode
\setlength\abovedisplayskip{2pt}
\setlength\belowdisplayskip{2pt}

\definecolor{darkblue}{RGB}{0,0,120}
\renewcommand{\ttdefault}{pcr}
\lstset{
  language=,
  basicstyle={\small\ttfamily\color{black}},
  numbers=none,
  %backgroundcolor=\color{lightgray},
  aboveskip=3mm,
  belowskip=3mm,
  showstringspaces=false,
  columns=flexible,
  keywordstyle={\bfseries\color{darkblue}},
  commentstyle={\color{red}\textit},
  stringstyle=\color{magenta},
  %frame=single,
  breaklines=true,
  breakatwhitespace=true,
  tabsize=4,
}

% The \icmltitle you define below is probably too long as a header.
% Therefore, a short form for the running title is supplied here:
\icmltitlerunning{\probkb: Managing Web-Scale Knowledge}

\begin{document} 

\twocolumn[
\icmltitle{Web-Scale Knowledge Inference Using Markov Logic Networks}

% It is OKAY to include author information, even for blind
% submissions: the style file will automatically remove it for you
% unless you've provided the [accepted] option to the icml2013
% package.
\icmlauthor{Yang Chen}{\href{mailto:yang@cise.ufl.edu}{yang@cise.ufl.edu}}
\icmlauthor{Daisy Zhe Wang}{\href{mailto:daisyw@cise.ufl.edu}{daisyw@cise.ufl.edu}}
\icmladdress{University of Florida, Dept of Computer Science,
  Gainesville, FL 32611 USA}

% You may provide any keywords that you 
% find helpful for describing your paper; these are used to populate 
% the "keywords" metadata in the PDF but will not be shown in the document
\icmlkeywords{machine learning, information extraction, databases}

\vskip 0.3in
]

\begin{abstract} 
In this paper, we present our on-going work on \probkb, a PROBabilistic
Knowledge Base constructed from web-scale extracted entities, facts, and
rules represented as a Markov logic network (MLN). We aim at web-scale
MLN inference by designing a novel relational model to represent MLNs
and algorithms that apply rules in batches. Errors are handled in a
principled and elegant manner to avoid error propagation and unnecessary
resource consumption. MLNs infer from the input a factor graph that
encodes a probability distribution over extracted and inferred facts. We
run parallel Gibbs sampling algorithms on Graphlab to query this
distribution. Initial experiment results show promising scalability of
our approach.
\end{abstract} 

\section{Introduction}
\label{sec:introduction}
With the exponential growth in machine learning, statistical inference
techniques, and big-data analytics frameworks, recent years have seen
tremendous research interest in automatic information extraction and
knowledge base construction. A knowledge base stores entities, facts,
and their relationships in a machine-readable form so as to help
machines understand information from human.

Currently, most popular techniques used to acquire knowledge include
automatic information extraction (IE) from text
corpus~\cite{carlson2010toward,poon2010machine,schoenmackers2010learning}
and massive human collaboration (Wikepedia, Freebase, etc). Though they
proved their success in a broad range of applications, there is still
much improvement we can make by \emph{making inference}. For example,
Wikipedia pages state that Kale is very high in calcium and calcium
helps prevent Osteoporosis, then we can infer that Kale helps prevent
Osteoporosis. However, this information is stated in neither page and
only be discovered by making inference.

Existing IE works extract entities, facts, and rules automatically from
the web~\cite{fader2011identifying,schoenmackers2010learning}, but due
to corpus noise and the inherent probabilistic nature of the learning
algorigthms, most of these extractions are uncertain. Our goal is to
facilitate inference over such noisy, uncertain extractions in a
principled, probabilistic, and scalable manner. To achieve this, we use
Markov logic networks (MLNs)~\cite{richardson2006markov}, an extension
of first-order logic that augments each clause with a
\emph{weight}. Clauses with finite weight are allowed to be violated,
but with a penalty determined by the weight. Together with all extracted
entities and facts, the MLN defines a Markov network (factor graph) that
encodes a probability distribution over all inferred
facts. Probabilistic inference is thus supported by querying this
distribution.

The main challenge related to MLNs is \emph{scalability}. The
state-of-the-art implementation, \tuffy~\cite{niu2011tuffy} and
\felix~\cite{niu2012scaling}, partially solved this problem by using
relational databases and task specialization. However, their systems
work well only on a small set of relations and hand-crafted MLNs, and
were not able to scale up to the web-scale datasets like \reverb and
\sherlock (See Section~\ref{sec:exp}). To gain this scalability, we
design a relational model that pushes all facts and rules into the
database. In this way, the grounding is reduced to a few joins among
these tables and the rules are thus applied in batches. This helps
achieve much greater scalability than \tuffy and \felix.

The second challenge stems from extraction errors. Most IE systems
assign a score to each of their extractions to indicate their fidelity
so they typically don't do any further cleaning. This poses a
significant challenge to inference engines: errors propagate rapidly and
violently without constraints. Figure~\ref{fig:errors} shows an example
of such propagation: starting from a single error\footnote{See
  \url{http://en.wikipedia.org/w/index.php?title=Statement_(logic)&diff=545338546&oldid=269607611}}
stating that Paris is the capital of Japan, a whole bunch of incorrect
results are produced.

\begin{figure*}[ht]
  \centering
  \includegraphics[clip,trim=36 40 45 45,width=\textwidth]{errors.pdf}
  \caption{Error propagation: How a single error source generates
    multiple errors and how they propagate furthur. Errors come
    different sources including incorrect extractions, rules, and
    propagated errors. The fact that Paris is the capital of Japan is
    extracted from a Wikipedia page describing logical statements. All
    base and derived errors are shown in red.}
  \label{fig:errors}
\end{figure*}

Errors come from diverse sources: incorrectly
extracted entities and facts, wrong rules, ambiguity, inference, etc. In
each rule application, with a single erroneous facts participating, the
result is likely to be erroneous. In a worse scenario, even if the
extractions are correct, errors may arise due to word
ambiguity. Removing errors early is a crucial task in knowledge base
construction; it improves knowledge quality and saves computation
resources for high-quality inferences.

This paper introduces the \probkb system that aims at tackling these
challenges and presents our initial results over a web-scale extraction
dataset.

\section{The \probkb System}
\label{sec:probkb}
This section presents our on-going work on \probkb. We designed a
relational model to represent the extractions and the MLN and designed
grounding algorithms in SQL that applies MLN clauses in batches. We
implemented it on Greenplum, a massive parallel processing (MPP)
database system. Inference is done by a parallel Gibbs
sampler~\cite{gonzalez2011parallel} on
GraphLab~\cite{Low+al:uai10graphlab}. The overview of the system
architecture is shown in Figure~\ref{fig:architecture}.

%\setlength{\textfloatsep}{20pt}
\begin{figure}[!h]
  \centering
  \includegraphics[width=\columnwidth]{architecture.pdf}
  \caption{\probkb architecture}
  \label{fig:architecture}
\end{figure}

The remaining of this section is structured as follows:
Section~\ref{sec:horn} justifies an important assumption regarding Horn
clauses. Section~\ref{sec:relational} introduces our relational model
for MLNs. Section~\ref{sec:grounding} presents the grounding algorithms
in terms of our relational model. Section~\ref{sec:errors} desribes our
initial work on maintaining knowledge
integrity. Section~\ref{sec:inference} describes our inference engine
built on GraphLab. Experiment results show \probkb scales much better
than the state-of-the-art.

\subsection{First-Order Horn Clauses}
\label{sec:horn}
A first-order \emph{Horn clause} is a clause with at most one positive
literal\footnote{\url{http://en.wikipedia.org/wiki/Horn_clause}}. In
this paper, we focus on this class of clauses only, though in general
Markov logic supports arbitrary forms. This restriction is reasonable in
our context since our goal to discover implicit knowledge from explicit
statements in the text corpus; this type of inference is mostly
expressed as ``if... then...'' statements in human language,
corresponding to the set of Horn clauses. Moreover, due to their simple
structure, Horn clauses are more easily to learn and be represented in a
structured form than general ones. We used a set of extracted Horn
clauses from \sherlock~\cite{schoenmackers2010learning}.

\subsection{The Relational MLN Model}
\label{sec:relational}
We represent the knowledge base as a relational model. Though previous
approaches already used relational databases to perform
grounding~\cite{niu2011tuffy,niu2012scaling}, the MLN model and
associating weights are stored in an external file. During runtime, a
SQL query is constructed for each individual rule using a host
language. This approach is inefficient when there are a large number of
rules. Our approach, on the contrary, stores the MLN in the database
so that the rules are applied in batches using joins. The only component
that the database does not efficiently support a probabilistic inference
engine that needs many random accesses to the input data. This component
is discussed in Section~\ref{sec:inference}.

Based on the assumptions made in Section~\ref{sec:horn}, we considered
Horn clauses only. We classified \sherlock rules according to their
sizes and argument orders. We identified six rule patterns in the
dataset:

% spacing in display mode
\setlength\abovedisplayskip{3pt}
\setlength\belowdisplayskip{5pt}
\begin{align}
p(x,y)&\leftarrow q(x,y)\label{eq:mln1}\\
p(x,y)&\leftarrow q(y,x)\label{eq:mln2}\\
p(x,y)&\leftarrow q(x,z), r(y,z)\label{eq:mln3}\\
p(x,y)&\leftarrow q(x,z), r(z,y)\label{eq:mln4}\\
p(x,y)&\leftarrow q(z,x), r(y,z)\label{eq:mln5}\\
p(x,y)&\leftarrow q(z,x), r(z,y)\label{eq:mln6}\\
\shortintertext{where $p$, $q$, $r$ are predicates and $x$, $y$, $z$ are
  variables. Each rule type $i$ has a table $M_i$ recording the
  predicates involved in the rules of that type. For each
  rule}
p(x,y)&\leftarrow q(x,y)\tag{1}\\
\shortintertext{of type~\ref{eq:mln1}, we have a
  tuple $(p,q)$ in $M_1$. For each rule}
p(x,y)&\leftarrow q(x,z), r(y,z)\tag{3}
\end{align}
of type~\ref{eq:mln3}, we have a tuple $(p,q,r)$ in $M_3$. We construct
$M_2$, $M_4$, $M_5$, and $M_6$ similarly. These tables record the
predicates only. The argument orders are implied by the rule types.

Longer rules may cause a problem since the number of rule patterns grows
exponentially with the rule size, making it impractical to create a
table for each of them. We leave this extension as a future work, but
our intuition is to record the arguments and use UDF to construct SQL
queries.

We have another table $R$ for relationships. For each relationship
$p(x,y)$ that is stated in the text corpus, we have a tuple $(p,x,y)$ in
$R$. The grounding algorithm is then easily expressed as equi-joins
between the $R$ table and $M_i$ tables as discussed next.

\subsection{Grounding}
\label{sec:grounding}
\eat{Following \alchemy\footnote{\url{http://alchemy.cs.washington.edu}}
  and \tuffy~\cite{niu2012scaling}, we used lazy
  inference~\cite{singla2006memory} and adopted a one-step look-ahead
  strategy: assume all atoms are inactive and compute active clauses;
  \emph{activate} the atoms in the grounding result and recompute active
  clauses. The detailed algorithm description could be found
  in~\cite{singla2006memory}.}

We use type~\ref{eq:mln3} rules to illustrate the grounding algorithm in
SQL.
\begin{align}
p(x,y)&\leftarrow q(x,z), r(y,z)\tag{3}
\end{align}
Assume these rules are stored in table $M_3(p,q,r)$, and relationships
$p(x,y)$ are stored in $R(p,x,y)$, then the following SQL query infers
new facts given a set of extracted and already inferred facts:
\begin{lstlisting}[language=SQL]
SELECT DISTINCT M3.p AS p,
         R1.x AS x, R2.y AS y
FROM M3 JOIN R R1 ON M3.q = R1.p
           JOIN R R2 ON M3.r = R2.p
WHERE R1.y = R2.y;
\end{lstlisting}\vspace{-8pt}
This process is repeated until convergence (i.e. no more facts can be
inferred). Then the following SQL query then generates the factors:
\begin{lstlisting}[language=SQL]
SELECT DISTINCT
    R1.id AS id1, R2.id AS id2, R3.id AS id3
FROM M3 JOIN R R ON M3.p = R.p
           JOIN R R1 ON M3.q = R1.p
           JOIN R R2 ON M3.r = R2.p
WHERE R.x = R1.x AND R.y = R2.x AND R1.y = R2.y;
\end{lstlisting}\vspace{-8pt}
To see why it is more efficient than \tuffy and \alchemy, consider the
first query. Suppose we first compute $RM_3\coloneqq
M_3\bowtie_{M_3.q=R.p}R$. Since the $M_i$ tables are often small, we use
a one-pass join algorithm and hash $M_3$ using $q$. Then when each tuple
$(p,x,y)$ in $R$ is read, it is matched against all rules in $M_3$ with
the first argument being $p$. In the second join $RM_3\bowtie_{RM3.r=R.p
  \texttt{ AND } RM3.y=R.y} R$, since $RM_3$ is typically much larger
than $M_3$, we assume using a two-pass hash-join algorithm, which starts
by hasing $RM_3$ and $R$ into buckets using keys $(r,y)$ and $(p,y)$,
respectively. Then, for any tuple $(p,x,y)$ in $R$, all possible results
can be formed in one pass by considering tuples from $RM_3$ in the
corresponding bucket. As a result, each tuple is read into memory for at
most 3 times and rules are simultaneously applied. This is in sharp
contrast with \tuffy, where tuples have to be read into main memory as
many times as the relation appears in the rules.

Our performance gain over \tuffy owes also to the simple syntax of Horn
clauses. In order to support general first-order clauses, \tuffy
materializes each possible grounding of all clauses to identify a set of
active clauses~\cite{singla2006memory}\footnote{For illustration,
  consider a non-Horn clause $\forall x\forall y(p(x,y)\vee q(x,y))$, or
  $\exists xp(x)$. These clauses can be made unsatisfied only by
  considering all possible assignments for $x$ and $y$.}. This process
quickly uses up disk space when trying to ground a dataset with large
numbers of entities, relations and clauses like \sherlock. Our
algorithms avoid this time- and space-consuming process by taking
advantage of simplicity of Horn clauses.

The result of grounding is a \emph{factor graph (Markov network)}. This
graph encodes a probability distribution over its variable nodes, which
can be used to answer user queries. However, marginal inference in
Markov networks is \#P-complete~\cite{roth1996hardness}, so we turn to
approximate inference methods. The state-of-the-art marginal inference
algorithm is MC-SAT~\cite{poon2006sound}, but due to absence of
deterministic rules and access to efficient parallel implementations of
the widely-adopted Gibbs sampler, we sticked to it and present an
initial evaluation in Section~\ref{sec:exp.inference}.

\subsection{Knowledge Integrity}
\label{sec:errors}
\cite{schoenmackers2010learning} observed a few common error patterns
(ambiguity, meaningless relations, etc) that might affect learning
quality and tried to remove these errors to obtain a cleaner corpus to
work on. This manual pruning strategy is useful, but not enough, for an
inference engine since errors often arise and propagate in unexpected
manners that are hard to describe systematically. For example, the fact
``Paris is the capital of Japan'' shown in Figure~\ref{fig:errors} is
accidentally extracted from a Wikipedia page that describes logical
statement, and no heuristic in~\cite{schoenmackers2010learning} is able
to filter it out. Unfortunately, even a single and unfrequent error like
this propagates rapidly in the inference chain. These errors hamper
knowledge quality, waste computation resources, and are hard to catch.

Instead of trying to enumerate common error patterns, we feel that it is
much easier to identify correct inferences: facts that are extracted
from multiple sources, or inferred by facts from multiple sources are
more likely to be correct than others. New errors are less likely to
arise when we applied rules to this subset of facts. Thus, we split the
database for the facts, moving the qualified facts to another table
called \emph{beliefs}, and we call the remaining facts
\emph{candidates}. Candidates are \emph{promoted} to beliefs if we
become confident about their fidelity. The terminologies are borrowed
from \nell~\cite{carlson2010toward}, but we are solving different
problems: we are trying to identify a \emph{computational-safe} subset
of our knowledge base so that we can safely apply the rules with no
errors propagating.

To save computation even further, we adapt this workflow to a semi-naive
query evaluation algorithm, which we call \emph{robust semi-naive
  evalution} to emphasize the fact that inferences only occur among most
correst facts and errors are unlikely to arise. Semi-naive
query evaluation originates from Datalog
literature~\cite{balbin1987generalization}; the basic idea is to avoid
repeated rule applications by restricting one operand to containing the
\emph{delta} records between two iterations.

\begin{algorithm}[ht]
   \caption{Robust Semi-Naive Evaluation}
   \label{alg:semi-naive}
\begin{algorithmic}
   \STATE \stt{candidates} = all facts
   \STATE \stt{beliefs} = \stt{promote($\varnothing$,candidates)}
   \STATE \stt{delta} = $\varnothing$
   \REPEAT
     \STATE \stt{promoters} = \stt{infer(beliefs,delta)}
     \STATE \stt{beliefs} = \stt{beliefs }$\cup$\stt{ delta}
     \STATE \stt{delta} = \stt{promote(promoters,candidates)}
   \UNTIL{\stt{delta}$=\varnothing$}
\end{algorithmic}
\end{algorithm}

In Algorithm~\ref{alg:semi-naive}, \stt{infer} is almost the same as
discussed in Section~\ref{sec:grounding}, except that the operands are
replaced by \stt{beliefs} and \stt{delta}, potentially much smaller than
the original $R$. The \stt{promote} algorithm is what we are still
working on. It takes a new set of inference results and uses them to
promote candidates to beliefs. Our intuition was to exploit
\emph{lineage} and promote facts implied by most external sources or to
learn a set of constraints to detect erroneous rules, facts, and join
keys.

\eat{Before closing this section, we note that the semi-naive evaluation
strategy may be applied to other problems like}

\subsection{The GraphLab Inference Engine}
\label{sec:inference}
The state-of-the-art MLN inference algorithm is
MC-SAT~\cite{singla2006memory}. For an initial evaluation and access to
existing libraries, though, our prototype used a parallel Gibbs
sampler~\cite{gonzalez2011parallel} implemented on the
GraphLab~\cite{Low+al:uai10graphlab,low2012distributed}
framework. GraphLab is a distributed framework that improves upon
abstractions like MapReduce for asynchronous iterative algorithms with
sparse computational dependencies while ensuring data consistency and
achieving a high degree of parallel performance. We ran the parallel
Gibbs algorithm on our grounded factor graph and got better results than
\tuffy.

Though the performance looks good, one concern we have is coordinating
the two systems. Our initial goal is to build a coherent system that
works on MLN inference problem. Getting two independent systems to
work together is hard and error-prone. Synchronization becomes
especially troublesome: to query even a single atom, we need to get
output from GraphLab, write the results to the database, and
perform a database query to get the result. Motivated by this, we are
trying to work out a \emph{shared memory model} that pushes
external operators directly into the database. In
Section~\ref{sec:exp.inference}, we only present results from GraphLab.

\section{Experiments}
\label{sec:exp}
We used extracted entities and facts from
\reverb~\cite{fader2011identifying} and applied \sherlock
rules~\cite{schoenmackers2010learning} to discover implicit
knowledge. \sherlock uses \textrunner~\cite{etzioni2008open}
extractions, an older version of \reverb, to learn the rules, so there
is a schema mismatch between the two. We are working on resolving this
issue by either mapping the \reverb schema to \sherlock's or
implementing our own rule learner for \reverb, but for now we use 50K out
of 400K extracted facts to present the initial results. The statistics
of our dataset is shown in Table~\ref{tab:sherlock}.
\begin{table}[ht]
  \centering
  \begin{tabular}{cc}\toprule
   \#entities  & 480K\\
   \#relations & 10K\\
   \#facts     & 50K\\
   \#rules     & 31K\\
   \bottomrule
  \end{tabular}
  \caption{\reverb-\sherlock data statistics.}
  \label{tab:sherlock}
\end{table}

\paragraph{Experiment Setup}
We ran all the experiments on a 32-core machine at 1400MHz with 64GB of
RAM running Red Hat Linux 4. \probkb, \tuffy, and GraphLab are
implemented in SQL, SQL (using Java as a host language), and C++,
respectively. The database system we use is Greenplum 4.2.

\subsection{Grounding}
\label{sec:exp.grounding}
We used \tuffy as a baseline comparison. Before grounding, we remove
some ambiguous mentions, including common first or last names and
general class references~\cite{schoenmackers2010learning}. The resulting
dataset has 7K relationships. We run \probkb and \tuffy using
this cleaner dataset and learn 100K new facts. Performance result is
shown in Table~\ref{tab:grounding}.

\begin{table}[ht]
  \centering
  \begin{tabular}{cc}\toprule
    \textbf{System} & \textbf{Time/s}\\\midrule
    \probkb & 85\\
    \tuffy & Crash\\\bottomrule
  \end{tabular}
  \caption{Grounding time for \probkb and \tuffy.}
  \label{tab:grounding}
\end{table}

As discussed in Section~\ref{sec:grounding}, the reason for our
performance improvement is the Horn clauses assumption and batch rule
application. In order to support general MLN inference, \tuffy
materializes all ground atoms for all predicates. This makes the active
closure algorithm efficient but consumes too much disk space. For a
dataset with large numbers of entities and relations like
\reverb-\sherlock, the disk space is used up even before the grounding
starts.

\subsection{Inference}
\label{sec:exp.inference}
This section reports \probkb's performance for inference using
GraphLab. Again we used \tuffy as comparison. Since \tuffy cannot ground
the whole \reverb-\sherlock dataset, we sampled a subset with 700 facts
and compared the time spent on generating 200 joint samples for \probkb
and \tuffy. The result is shown in Table~\ref{tab:inference}.

\begin{table}[ht]
  \centering
  \begin{tabular}{cc}\toprule
    \textbf{System} & \textbf{Time/min}\\\midrule
    \probkb & 0.47\\
    \tuffy  & 55\\\bottomrule
  \end{tabular}
  \caption{Time to generate 200 joint samples.}
  \label{tab:inference}
\end{table}

The performance boost owes mostly to the GraphLab parallel engine.

The experimental results presented in this section are preliminary, but
it shows the need for better grounding and inference system to achieve
web scale and the promise of our proposed techniques to solve the
scalability and integrity challenges.

\section{Conclusion and Future Work}
This paper presents our on-going work on \probkb. We built an initial
prototype that stores MLNs in a relational form and designed an
efficient grounding algorithm that applies rules in batches. We
maintained a computational-safe set of ``beliefs'' to which rules are
applied with minimal errors occurring. We connect to GraphLab for a
parallel Gibbbs sampler inference engine. Future works are discussed
throughout the paper and are summarized below:
\begin{itemize}[noitemsep,topsep=0pt,parsep=0pt,partopsep=0pt]
  \item Develop techniques for the robust semi-naive algorithm to
    maintain knowledge integrity.
  \item Tightly integrate grounding and inference over MLN using
    shared-memory model.
  \item Port SQL implementation of grounding to other frameworks, such
    as Hive and Shark.
\end{itemize}
\newpage

% In the unusual situation where you want a paper to appear in the
% references without citing it in the main text, use \nocite
\bibliography{probkb}
\bibliographystyle{icml2013}

\end{document} 


% This document was modified from the file originally made available by
% Pat Langley and Andrea Danyluk for ICML-2K. This version was
% created by Lise Getoor and Tobias Scheffer, it was slightly modified  
% from the 2010 version by Thorsten Joachims & Johannes Fuernkranz, 
% slightly modified from the 2009 version by Kiri Wagstaff and 
% Sam Roweis's 2008 version, which is slightly modified from 
% Prasad Tadepalli's 2007 version which is a lightly 
% changed version of the previous year's version by Andrew Moore, 
% which was in turn edited from those of Kristian Kersting and 
% Codrina Lauth. Alex Smola contributed to the algorithmic style files.  
